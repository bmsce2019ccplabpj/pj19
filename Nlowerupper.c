#include <stdio.h>
int main()
{
    char ch;
    int numsum=0,uppersum=0,lowersum=0;
    do{
        printf("ENTER CHARACTER (STOPS ONLY IF * ENTERED) ");
        scanf(" %c",&ch);
        if(ch>=48&&ch<=57)
            numsum+=1;
        else if(ch>=65&&ch<=90)
            uppersum+=1;
        else if(ch>=97&&ch<=122)
            lowersum+=1;
    }while(ch!='*');
    printf("\nTHE NUMBER OF UPPERCASE CHARACTERS ENTERED IS %d",uppersum);
    printf("\nTHE NUMBER OF LOWERCASE CHARACTERS ENTERED IS %d",lowersum);
    printf("\nTHE NUMBER OF NUMERICAL CHARACTERS ENTERED IS %d",numsum);
    return 0;
}
