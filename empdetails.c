#include <stdio.h>
struct date
{
    int day;
    int month;
    int year;
};
struct employee
{
   char name[50];
   int emp_id;
   float salary;
   struct date date_of_join;
};
int main()
{
    struct employee emp1;
    printf("\nENTER YOUR NAME ");
    gets(emp1.name);
    printf("\nENTER YOUR EMPLOYEE ID ");
    scanf("%d",&emp1.emp_id);
    printf("\nENTER YOUR SALARY ");
    scanf("%f",&emp1.salary);
    printf("\nENTER YOUR DATE OF JOIN ");
    printf("\nENTER YOUR DAY OF JOIN (eg:23) ");
    scanf("%d",&emp1.date_of_join.day);
    printf("\nENTER YOUR MONTH OF JOIN (eg:8) ");
    scanf("%d",&emp1.date_of_join.month);
    printf("\nENTER YOUR YEAR OF JOIN (eg:2012) ");
    scanf("%d",&emp1.date_of_join.year);
    //NOW TO DISPLAY DETAILS
    printf("\nYOUR NAME IS ");
    puts(emp1.name);
    printf("\nYOUR EMPLOYEE ID IS %d",emp1.emp_id);
    printf("\nYOUR SALARY IS %d",emp1.salary);
    printf("\nYOUR DATE OF JOINING IS ");
    printf("%d/%d/%d",emp1.date_of_join.day,emp1.date_of_join.month,emp1.date_of_join.year);
    return 0;
}

