#include<stdio.h>
int main()
{
    int size,i,ele,mid,pos=-1;
    printf("ENTER THE SIZE OF THE ARRAY ");
    scanf("%d",&size);
    int ar[size];
    printf("\nENTER THE ELEMENTS (IN ASCENDING OR DESCENDING ORDER) ");
    for(i=0;i<size;++i)
        scanf("%d",&ar[i]);
    printf("\nENTER THE ELEMENT TO BE SEARCHED FOR ");
    scanf("%d",&ele);
    int beg=0;
    int end=size-1;
    while(beg<=end)
    {
        mid=((beg+end)/2);
        if(ele==ar[mid])
        {
            pos=mid+1;
            break;
        }
        else if(ele>ar[mid])
            beg=mid+1;
        else
            end=mid-1;
    }
    printf("\nTHE ARRAY IS ");
    for(i=0;i<size;++i)
        printf("%d ",ar[i]);
    if(pos==-1)
        printf("\nELEMENT WAS NOT FOUND ");
    else
        printf("\nELEMENT %d WAS FOUND IN %d PLACE",ar[pos-1],pos);
    return 0;
}
