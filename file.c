#include <stdio.h>
int main()
{
    char c;
    FILE *f1;
    f1=fopen("INPUT.txt","w");
    printf("ENTER THE DATA INTO THE FILE \n");
    while((c=getchar())!=EOF)
        fputc(c,f1);
    fclose(f1);
    printf("\nTHE FILE CONTENTS ARE \n");
    f1=fopen("INPUT.txt","r");
    while((c=fgetc(f1))!=EOF)
        putchar(c);
    fclose(f1);
    return 0;
}
