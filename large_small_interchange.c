#include <stdio.h>
int main()
{
    int n,i,large,small,largepos,smallpos;
    printf("ENTER THE SIZE OF THE ARRAY ");
    scanf("%d",&n);
    int ar[n];
    printf("\nENTER THE ELEMENTS ");
    for(i=0;i<n;++i)
        scanf("%d",&ar[i]);
    small=large=ar[0];
    largepos=smallpos=0;
    for(i=1;i<n;++i)
    {
        if(ar[i]>large)
        {
            large=ar[i];
            largepos=i;
        }
        if(ar[i]<small)
        {
            small=ar[i];
            smallpos=i;
        }
    }
    printf("\nTHE ARRAY BEFORE INTERCHANGE IS ");
    printf("\n");
    for(i=0;i<n;++i)
        printf("%d",ar[i]);
    printf("\nTHE POSITION OF LARGEST ELEMENT IS %d",(largepos+1));
    printf("\nTHE POSITION OF SMALLEST ELEMENT IS %d",(smallpos+1));
    printf("\nTHE LARGEST NUMBER IS %d",large);
    printf("\nTHE SMALLEST NUMBER IS %d",small);
    ar[smallpos]=large;
    ar[largepos]=small;
    printf("\nTHE ARRAY AFTER INTERCHANGE IS ");
    printf("\n");
    for(i=0;i<n;++i)
        printf("%d",ar[i]);
    return 0;
}
