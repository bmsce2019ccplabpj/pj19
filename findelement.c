#include <stdio.h>
int main()
{
    int i,n,ele,pos;
    printf("ENTER THE SIZE OF THE ARRAY YOU WANT TO ENTER ");
    scanf("%d",&n);
    int ar[n];
    printf("\nENTER THE ELEMENTS ");
    for(i=0;i<n;++i)
       scanf("%d",&ar[i]);
    printf("\nENTER THE ELEMENT YOU WANT TO FIND ");
    scanf("%d",&ele);
    for(i=0;i<n;++i)
        if(ar[i]==ele)
        {
           pos=i+1;
           break;
        }
    printf("\nTHE ARRAY IS ");
    for(i=0;i<n;++i)
        printf("%d",ar[i]);
    printf("\nTHE POSITION OF %d ELEMENT IS %d",ele,pos);
    return 0;
}
