#include <stdio.h>
void swap(int *x,int *y)
{
   int temp;
   temp=*x;
   *x=*y;
   *y=temp;
}
int main()
{
    int a,b;
    printf("ENTER THE FIRST VALUE ");
    scanf("%d",&a);
    printf("\nENTER THE SECOND VALUE ");
    scanf("%d",&b);
    printf("\nBEFORE SWAPPING ");
    printf("%d %d",a,b);
    swap(&a,&b);
    printf("\nNOW AFTER SWAPPING ");
    printf("%d %d",a,b);
    return 0;
}
